<?php die(); ?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" lang="zh_CN">
<![endif]-->
<!--[if IE 7]>
<html id="ie7" lang="zh_CN">
<![endif]-->
<!--[if IE 8]>
<html id="ie8" lang="zh_CN">
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html lang="zh_CN">
<!--<![endif]-->
<head>
	<meta charset="UTF-8" />
			
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="pingback" href="http://p.lvovl.cn/xmlrpc.php" />

		<!--[if lt IE 9]>
	<script src="http://p.lvovl.cn/wp-content/themes/Divi/js/html5.js" type="text/javascript"></script>
	<![endif]-->

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<script>var et_site_url='http://p.lvovl.cn';var et_post_id='9';function et_core_page_resource_fallback(a,b){"undefined"===typeof b&&(b=a.sheet.cssRules&&0===a.sheet.cssRules.length);b&&(a.onerror=null,a.onload=null,a.href?a.href=et_site_url+"/?et_core_page_resource="+a.id+et_post_id:a.src&&(a.src=et_site_url+"/?et_core_page_resource="+a.id+et_post_id))}
</script><title>aaa | Gavin&#039;s 钢琴笔记</title>
<link rel='dns-prefetch' href='//pcdn.lvovl.cn' />
<link rel="alternate" type="application/rss+xml" title="Gavin&#039;s 钢琴笔记 &raquo; Feed" href="http://p.lvovl.cn/feed/" />
<link rel="alternate" type="application/rss+xml" title="Gavin&#039;s 钢琴笔记 &raquo; Comments Feed" href="http://p.lvovl.cn/comments/feed/" />
<meta content="Divi v.3.0.106" name="generator"/><link rel='stylesheet' id='divi-style-css'  href='http://p.lvovl.cn/wp-content/themes/Divi/style.dev.css?ver=3.0.106' type='text/css' media='all' />
<link rel='stylesheet' id='et-shortcodes-responsive-css-css'  href='http://p.lvovl.cn/wp-content/themes/Divi/epanel/shortcodes/css/shortcodes_responsive.css?ver=3.0.106' type='text/css' media='all' />
<link rel='stylesheet' id='magnific-popup-css'  href='http://p.lvovl.cn/wp-content/themes/Divi/includes/builder/styles/magnific_popup.css?ver=3.0.106' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='http://p.lvovl.cn/wp-includes/css/dashicons.min.css?ver=4.9.8' type='text/css' media='all' />
<script type='text/javascript' src='//dn-staticfile.qbox.me/jquery/1.11.1/jquery.min.js?ver=1.11.1'></script>
<script type='text/javascript' src='//dn-staticfile.qbox.me/jquery-migrate/1.2.1/jquery-migrate.min.js?ver=1.2.1'></script>
<link rel="canonical" href="http://p.lvovl.cn/2018/10/16/aaa/" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" /><link rel="stylesheet" id="et-divi-customizer-global-cached-inline-styles" href="http://p.lvovl.cn/wp-content/cache/et/global/et-divi-customizer-global-15396826498551.min.css" onerror="et_core_page_resource_fallback(this, true)" onload="et_core_page_resource_fallback(this)" /></head>
<body class="post-template-default single single-post postid-9 single-format-standard logged-in et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter osx et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_right_sidebar et_divi_theme">
	<div id="page-container">

	
	
		<header id="main-header" data-height-onload="66">
			<div class="container clearfix et_menu_container">
							<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="http://p.lvovl.cn/">
						<img src="http://p.lvovl.cn/wp-content/themes/Divi/images/logo.png" alt="Gavin&#039;s 钢琴笔记" id="logo" data-height-percentage="54" />
					</a>
				</div>
				<div id="et-top-navigation" data-height="66" data-fixed-height="40">
											<nav id="top-menu-nav">
													<ul id="top-menu" class="nav">
																	<li ><a href="http://p.lvovl.cn/">Home</a></li>
								
								<li class="page_item page-item-2"><a href="http://p.lvovl.cn/sample-page/">示例页面</a></li>
									<li class="cat-item cat-item-1"><a href="http://p.lvovl.cn/category/uncategorized/" >未分类</a>
</li>
							</ul>
												</nav>
					
					
					
										<div id="et_top_search">
						<span id="et_search_icon"></span>
					</div>
					
					<div id="et_mobile_nav_menu">
				<div class="mobile_nav closed">
					<span class="select_page">Select Page</span>
					<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
				</div>
			</div>				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
			<div class="et_search_outer">
				<div class="container et_search_form_container">
					<form role="search" method="get" class="et-search-form" action="http://p.lvovl.cn/">
					<input type="search" class="et-search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:" />					</form>
					<span class="et_close_search_field"></span>
				</div>
			</div>
		</header> <!-- #main-header -->

		<div id="et-main-area">

<div id="main-content">
		<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
											<article id="post-9" class="et_pb_post post-9 post type-post status-publish format-standard hentry category-uncategorized">
											<div class="et_post_meta_wrapper">
							<h1 class="entry-title">aaa</h1>

						<p class="post-meta"> by <span class="author vcard"><a href="http://p.lvovl.cn/author/gavin/" title="Posts by Gavin" rel="author">Gavin</a></span> | <span class="published">Oct 16, 2018</span> | <a href="http://p.lvovl.cn/category/uncategorized/" rel="category tag">未分类</a> | <span class="comments-number"><a href="http://p.lvovl.cn/2018/10/16/aaa/#respond">0 comments</a></span></p>
												</div> <!-- .et_post_meta_wrapper -->
				
					<div class="entry-content">
					<p><img class="alignnone size-medium wp-image-8" src="http://pcdn.lvovl.cn/qiniu/9/image/b3448b7a2bbb914dce16c5464149cabb.jpg" alt="" width="225" height="300" /><br />
<img src="http://pcdn.lvovl.cn/qiniu/9/image/18f08d0e93ae89297a9e7a2a8493fdbe.jpg" alt="" width="300" height="225" class="alignnone size-medium wp-image-13" /></p>
					</div> <!-- .entry-content -->
					<div class="et_post_meta_wrapper">
					
					
					<!-- You can start editing here. -->

<section id="comment-wrap">
		   <div id="comment-section" class="nocomments">
		  			 <!-- If comments are open, but there are no comments. -->

		  	   </div>
					<div id="respond" class="comment-respond">
		<h3 id="reply-title" class="comment-reply-title"><span>Submit a Comment</span> <small><a rel="nofollow" id="cancel-comment-reply-link" href="/2018/10/16/aaa/#respond" style="display:none;">Cancel reply</a></small></h3>			<form action="http://p.lvovl.cn/wp-comments-post.php" method="post" id="commentform" class="comment-form">
				<p class="logged-in-as"><a href="http://p.lvovl.cn/wp-admin/profile.php" aria-label="Logged in as Gavin. Edit your profile.">Logged in as Gavin</a>. <a href="http://p.lvovl.cn/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Fp.lvovl.cn%2F2018%2F10%2F16%2Faaa%2F&amp;_wpnonce=b1fd1433e7">Log out?</a></p><p class="comment-form-comment"><label for="comment">Comment</label> <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" required="required"></textarea></p><p class="form-submit"><input name="submit" type="submit" id="submit" class="submit et_pb_button" value="Submit Comment" /> <input type='hidden' name='comment_post_ID' value='9' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
</p><input type="hidden" id="_wp_unfiltered_html_comment_disabled" name="_wp_unfiltered_html_comment_disabled" value="b07392386f" /><script>(function(){if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}})();</script>
			</form>
			</div><!-- #respond -->
		</section>					</div> <!-- .et_post_meta_wrapper -->
				</article> <!-- .et_pb_post -->

						</div> <!-- #left-area -->

				<div id="sidebar">
		<div id="search-2" class="et_pb_widget widget_search"><form role="search" method="get" id="searchform" class="searchform" action="http://p.lvovl.cn/">
				<div>
					<label class="screen-reader-text" for="s">Search for:</label>
					<input type="text" value="" name="s" id="s" />
					<input type="submit" id="searchsubmit" value="Search" />
				</div>
			</form></div> <!-- end .et_pb_widget -->		<div id="recent-posts-2" class="et_pb_widget widget_recent_entries">		<h4 class="widgettitle">Recent Posts</h4>		<ul>
											<li>
					<a href="http://p.lvovl.cn/2018/10/16/fff/">fff</a>
									</li>
											<li>
					<a href="http://p.lvovl.cn/2018/10/16/aaa/">aaa</a>
									</li>
											<li>
					<a href="http://p.lvovl.cn/2018/10/15/hello-world/">世界，您好！</a>
									</li>
					</ul>
		</div> <!-- end .et_pb_widget --><div id="recent-comments-2" class="et_pb_widget widget_recent_comments"><h4 class="widgettitle">Recent Comments</h4><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link">Gavin</span> on <a href="http://p.lvovl.cn/2018/10/15/hello-world/#comment-2">世界，您好！</a></li><li class="recentcomments"><span class="comment-author-link"><a href='https://wordpress.org/' rel='external nofollow' class='url'>一位WordPress评论者</a></span> on <a href="http://p.lvovl.cn/2018/10/15/hello-world/#comment-1">世界，您好！</a></li></ul></div> <!-- end .et_pb_widget --><div id="archives-2" class="et_pb_widget widget_archive"><h4 class="widgettitle">Archives</h4>		<ul>
			<li><a href='http://p.lvovl.cn/2018/10/'>October 2018</a></li>
		</ul>
		</div> <!-- end .et_pb_widget --><div id="categories-2" class="et_pb_widget widget_categories"><h4 class="widgettitle">Categories</h4>		<ul>
	<li class="cat-item cat-item-1"><a href="http://p.lvovl.cn/category/uncategorized/" >未分类</a>
</li>
		</ul>
</div> <!-- end .et_pb_widget --><div id="meta-2" class="et_pb_widget widget_meta"><h4 class="widgettitle">Meta</h4>			<ul>
			<li><a href="http://p.lvovl.cn/wp-admin/">Site Admin</a></li>			<li><a href="http://p.lvovl.cn/wp-login.php?action=logout&#038;_wpnonce=b1fd1433e7">Log out</a></li>
			<li><a href="http://p.lvovl.cn/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a href="http://p.lvovl.cn/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li>			</ul>
			</div> <!-- end .et_pb_widget -->	</div> <!-- end #sidebar -->
		</div> <!-- #content-area -->
	</div> <!-- .container -->
	</div> <!-- #main-content -->


			<footer id="main-footer">
				

		
				<div id="footer-bottom">
					<div class="container clearfix">
				<ul class="et-social-icons">


</ul><p id="footer-info">Designed by <a href="http://www.elegantthemes.com" title="Premium WordPress Themes">Elegant Themes</a> | Powered by <a href="http://www.wordpress.org">WordPress</a></p>					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->


	</div> <!-- #page-container -->

		<script type="text/javascript">
		var et_animation_data = [];
	</script>
	<script type='text/javascript' src='http://p.lvovl.cn/wp-content/themes/Divi/includes/builder/scripts/frontend-builder-global-functions.js?ver=3.0.106'></script>
<script type='text/javascript' src='http://p.lvovl.cn/wp-includes/js/comment-reply.min.js?ver=4.9.8'></script>
<script type='text/javascript' src='http://p.lvovl.cn/wp-content/themes/Divi/includes/builder/scripts/jquery.mobile.custom.min.js?ver=3.0.106'></script>
<script type='text/javascript' src='http://p.lvovl.cn/wp-content/themes/Divi/js/custom.js?ver=3.0.106'></script>
<script type='text/javascript' src='http://p.lvovl.cn/wp-content/themes/Divi/includes/builder/scripts/jquery.fitvids.js?ver=3.0.106'></script>
<script type='text/javascript' src='http://p.lvovl.cn/wp-content/themes/Divi/includes/builder/scripts/waypoints.min.js?ver=3.0.106'></script>
<script type='text/javascript' src='http://p.lvovl.cn/wp-content/themes/Divi/includes/builder/scripts/jquery.magnific-popup.js?ver=3.0.106'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var et_pb_custom = {"ajaxurl":"http:\/\/p.lvovl.cn\/wp-admin\/admin-ajax.php","images_uri":"http:\/\/p.lvovl.cn\/wp-content\/themes\/Divi\/images","builder_images_uri":"http:\/\/p.lvovl.cn\/wp-content\/themes\/Divi\/includes\/builder\/images","et_frontend_nonce":"7322cc1473","subscription_failed":"Please, check the fields below to make sure you entered the correct information.","et_ab_log_nonce":"9a0094cd4c","fill_message":"Please, fill in the following fields:","contact_error_message":"Please, fix the following errors:","invalid":"Invalid email","captcha":"Captcha","prev":"Prev","previous":"Previous","next":"Next","wrong_captcha":"You entered the wrong number in captcha.","is_builder_plugin_used":"","ignore_waypoints":"no","is_divi_theme_used":"1","widget_search_selector":".widget_search","is_ab_testing_active":"","page_id":"9","unique_test_id":"","ab_bounce_rate":"5","is_cache_plugin_active":"yes","is_shortcode_tracking":""};
var et_pb_box_shadow_elements = [];
/* ]]> */
</script>
<script type='text/javascript' src='http://p.lvovl.cn/wp-content/themes/Divi/includes/builder/scripts/frontend-builder-scripts.js?ver=3.0.106'></script>
<script type='text/javascript' src='http://p.lvovl.cn/wp-content/themes/Divi/core/admin/js/common.js?ver=3.0.106'></script>
</body>
</html>
<!-- Dynamic page generated in 3.141 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2018-10-16 19:50:47 -->
